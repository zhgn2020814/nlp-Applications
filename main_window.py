import os
import os
import sys

from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget, QMessageBox
from PySide2.QtGui import QPixmap
from PySide2.QtUiTools import QUiLoader

from NER import hmm_ner, perceptron_ner, crf_ner
from classification import text_classification, svm_text_classification
from clear import RemoveDir
from cluster import k_means,Agglomerative_Clustering
from login2 import Ui_MainWindow
from wordsegmentation import wordsegmentation


class Ui_Form(QWidget):
    def __init__(self, *args, obj=None, **kwargs):
        super(Ui_Form, self).__init__(*args, **kwargs)
        self.ui = QUiLoader().load('untitled.ui')

    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(603, 367)
        self.textEdit = QtWidgets.QTextEdit(Form)
        self.textEdit.setGeometry(QtCore.QRect(10, 20, 571, 331))
        self.textEdit.setAccessibleName("")
        self.textEdit.setObjectName("textEdit")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "相关信息"))
        self.textEdit.setHtml(_translate("Form",
                                         "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                         "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                         "p, li { white-space: pre-wrap; }\n"
                                         "</style></head><body style=\" font-family:\'SimSun\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">版本号：1.0</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">发布日期：2023.5.9</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">开发者：ZhangHan</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">开发者邮箱：944211286@qq.com</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">(欢迎您将使用建议及问题反馈到开发者邮箱）</span></p>\n"
                                         "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))


class Window_main(QWidget, Ui_MainWindow):
    def __init__(self, *args, obj=None, **kwargs):
        super(Window_main, self).__init__(*args, **kwargs)
        self.is_on = 0
        self.is_on2 = 0
        self.is_on3 = 0
        self.ui2 = QUiLoader().load('login2.ui')
        # 界面切换
        self.ui2.wordsegmentation.clicked.connect(lambda: self.on_pushButton_clicked(self.ui2.wordsegmentation))
        self.ui2.NER.clicked.connect(lambda: self.on_pushButton_clicked(self.ui2.NER))
        self.ui2.classification.clicked.connect(lambda: self.on_pushButton_clicked(self.ui2.classification))
        self.ui2.cluster.clicked.connect(lambda: self.on_pushButton_clicked(self.ui2.cluster))
        # 分词界面触发函数
        self.ui2.pushButton_9.clicked.connect(self.wordsegmentation)
        # 命名实体识别触发函数
        self.ui2.pushButton_10.clicked.connect(self.Ner)
        # 变量
        self.file_path = ""
        self.wordsegmentation_input_text = ""
        self.wordsegmentation_output_text = ""
        self.Ner_input_text = ""
        self.Ner_output_text = ""
        self.Ner_model = 0
        self.ui2.radioButton.toggled.connect(lambda: self.choose_Ner_model(self.ui2.radioButton))
        self.ui2.radioButton_2.toggled.connect(lambda: self.choose_Ner_model(self.ui2.radioButton_2))
        self.ui2.radioButton_3.toggled.connect(lambda: self.choose_Ner_model(self.ui2.radioButton_3))
        self.classification_model = 0
        self.classification_input_text = ""
        self.classification_output_text = ""
        self.ui2.radioButton_4.toggled.connect(lambda: self.choose_classification_model(self.ui2.radioButton_4))
        self.ui2.radioButton_5.toggled.connect(lambda: self.choose_classification_model(self.ui2.radioButton_5))
        self.ui2.radioButton_6.toggled.connect(lambda: self.choose_classification_model(self.ui2.radioButton_6))
        self.cluster_output_text = ""
        self.cluster_output_file_path = ""
        self.cluster_output_file_txt = ""
        self.ui2.radioButton_7.toggled.connect(lambda: self.choose_cluster_model(self.ui2.radioButton_7))
        self.ui2.radioButton_8.toggled.connect(lambda: self.choose_cluster_model(self.ui2.radioButton_8))
        self.cluster_model = 0
        self.cluster_k = 2
        # 文本聚类
        self.ui2.toolButtonFive.clicked.connect(self.Get_File_Path)
        self.ui2.pushButton_12.clicked.connect(self.cluster)
        self.ui2.cluster_list.itemClicked.connect(self.onItemClicked)
        # 文本分类
        self.ui2.pushButton_13.clicked.connect(self.classification)
        # 清除缓存
        self.ui2.pushButton_6.clicked.connect(self.Clear_Cache)
        # 加载图片
        pixmap = QPixmap('images/welcome.jpg')
        self.ui2.label_15.setPixmap(pixmap)
        self.ui2.label_15.setMouseTracking(True)
        self.ui2.label_15.setStyleSheet('QLabel:hover { background-color: yellow }')
        # 版本
        self.ui2.actionban.triggered.connect(self.show_ban)
        self.child_window = Ui_Form()

    # 文本分类
    def classification(self):
        self.classification_input_text = self.ui2.plainTextEdit_6.toPlainText()
        # 朴素贝叶斯分类器
        if self.classification_model == 0:
            classifier = text_classification.train_or_load_classifier()
            self.classification_output_text = classifier.classify(self.classification_input_text)
            print(self.classification_output_text)
        # 线性支持向量机分类器
        elif self.classification_model == 1:
            classifier = svm_text_classification.train_or_load_classifier()
            self.classification_output_text = classifier.classify(self.classification_input_text)
            print(self.classification_output_text)
            pass
        # EasyBert分类器
        elif self.classification_model == 2:
            pass
        # 设置分类结果
        self.set_class_result(self.classification_output_text)

    # 上传文件获取文件路径
    def Get_File_Path(self):
        self.file_path, _ = QtWidgets.QFileDialog.getOpenFileName(self, '上传文件', '', '所有文件 (*.*);;文本文件 (*.txt)')
        if self.file_path:
            self.ui2.label_11.setText(self.file_path)

    # 文本聚类
    def cluster(self):
        if self.cluster_model == 0:
            self.cluster_k = int(self.ui2.lineEdit_2.text())
            self.cluster_output_text = k_means.test(self.file_path, self.cluster_k)
            self.ui2.plainTextEdit_5.setPlainText(self.cluster_output_text)
            self.cluster_output_file_path = self.file_path[:self.file_path.rfind('/') + 1] + "k_means"
            if self.cluster_output_file_path:
                files = os.listdir(self.cluster_output_file_path)
                self.ui2.cluster_list.clear()
                self.ui2.cluster_list.addItems(files)
            self.ui2.textEdit.clear()
        elif self.cluster_model == 1:
            self.cluster_k = int(self.ui2.lineEdit_2.text())
            self.cluster_output_text = Agglomerative_Clustering.test(self.file_path, self.cluster_k)
            self.ui2.plainTextEdit_5.setPlainText(self.cluster_output_text)
            self.cluster_output_file_path = self.file_path[:self.file_path.rfind('/') + 1] + "Agglomerative_Clustering"
            if self.cluster_output_file_path:
                files = os.listdir(self.cluster_output_file_path)
                self.ui2.cluster_list.clear()
                self.ui2.cluster_list.addItems(files)
            self.ui2.textEdit.clear()

    # 聚类结果列表文件查看
    def onItemClicked(self, item):
        self.cluster_output_file_txt = self.cluster_output_file_path + "/" + item.text()
        if self.cluster_output_file_txt:
            with open(self.cluster_output_file_txt, 'r', encoding='utf-8') as file:
                content = file.read()
        self.ui2.textEdit.setPlainText(content)

    # 分词
    def wordsegmentation(self):
        self.wordsegmentation_input_text = self.ui2.plainTextEdit.toPlainText()
        param = wordsegmentation.wordsegmentation_train('wordsegmentation/HMMTrainSet.txt')
        self.wordsegmentation_output_text = wordsegmentation.word_partition(param, self.wordsegmentation_input_text)
        self.ui2.plainTextEdit_2.setPlainText(str(self.wordsegmentation_output_text))

    # 命名实体识别
    def Ner(self):
        self.Ner_input_text = self.ui2.plainTextEdit_3.toPlainText()
        # 隐马尔可夫模型
        if self.Ner_model == 0:
            PKU98 = hmm_ner.ensure_data("pku98", "http://file.hankcs.com/corpus/pku98.zip")
            PKU199801_TRAIN = os.path.join(PKU98, '199801-train.txt')
            recognizer = hmm_ner.train(PKU199801_TRAIN)
            self.Ner_output_text = hmm_ner.test(recognizer, self.Ner_input_text)
            self.ui2.plainTextEdit_4.setPlainText(str(self.Ner_output_text))
        # 感知机模型
        elif self.Ner_model == 1:
            PKU98 = perceptron_ner.ensure_data("pku98", "http://file.hankcs.com/corpus/pku98.zip")
            PKU199801_TRAIN = os.path.join(PKU98, '199801-train.txt')
            NER_MODEL = os.path.join(PKU98, 'ner.bin')
            recognizer = perceptron_ner.train(PKU199801_TRAIN, NER_MODEL)
            self.Ner_output_text = perceptron_ner.test(recognizer, self.Ner_input_text)
            self.ui2.plainTextEdit_4.setPlainText(str(self.Ner_output_text))
        # 条件随机场模型
        elif self.Ner_model == 2:
            PKU98 = crf_ner.ensure_data("pku98", "http://file.hankcs.com/corpus/pku98.zip")
            PKU199801_TRAIN = os.path.join(PKU98, '199801-train.txt')
            NER_MODEL = os.path.join(PKU98, 'ner.bin')
            recognizer = crf_ner.train(PKU199801_TRAIN, NER_MODEL)
            self.Ner_output_text = crf_ner.test(recognizer, self.Ner_input_text)
            self.ui2.plainTextEdit_4.setPlainText(str(self.Ner_output_text))
        else:
            pass
    def set_class_result(self, class_id):
        # 设置分类结果按钮颜色
        buttons = [self.ui2.pushButtonEight_2, self.ui2.pushButtonFive_3, self.ui2.pushButtonFour_2,
                   self.ui2.pushButtonThree_2, self.ui2.pushButtonTwo_2]
        for button in buttons:
            if button.text() == class_id:
                button.setStyleSheet('background-color: green')
            else:
                button.setStyleSheet('')

    # 命名实体识别模型单按钮选择
    def choose_Ner_model(self, btn):
        if btn.text() == '隐马尔可夫模型':
            if btn.isChecked():
                self.ui2.textBrowser.append("<h4>" + btn.text() + "已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
                self.Ner_model = 0
        if btn.text() == "感知机模型":
            if btn.isChecked():
                self.ui2.textBrowser.append("<h4>" + btn.text() + "已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
                self.Ner_model = 1
        if btn.text() == "条件随机场模型":
            if btn.isChecked():
                self.Ner_model = 2
                self.ui2.textBrowser.append("<h4>" + btn.text() + "已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
    # 文本聚类模型单按钮选择
    def choose_cluster_model(self, btn):
        if btn.text() == 'k_means':
            if btn.isChecked():
                self.ui2.textBrowser.append("<h4>" + btn.text() + "已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
                self.cluster_model = 0
        if btn.text() == "层次聚类算法":
            if btn.isChecked():
                self.ui2.textBrowser.append("<h4>" + btn.text() + "已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
                self.cluster_model = 1

    # 文本分类模型单按钮选择
    def choose_classification_model(self, btn):
        if btn.text() == '朴素贝叶斯分类器':
            if btn.isChecked():
                self.ui2.textBrowser.append("<h4>" + btn.text() + "已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
                self.classification_model = 0
        if btn.text() == "线性支持向量机分类器":
            if btn.isChecked():
                self.ui2.textBrowser.append("<h4>" + btn.text() + "已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
                self.classification_model = 1
        if btn.text() == "EasyBert分类器":
            if btn.isChecked():
                self.classification_model = 2
                self.ui2.textBrowser.append("<h4>" + btn.text() + "已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本

    # 版本信息
    def show_ban(self):
        # self.help_thread.start()
        self.child_window.ui.show()

    # 清除缓存
    def Clear_Cache(self):
        a = QMessageBox.question(self, '提示', '你确定要清除所有数据缓存吗?', QMessageBox.Yes | QMessageBox.No,
                                 QMessageBox.No)  # "退出"代表的是弹出框的标题,"你确认退出.."表示弹出框的内容
        if a == QMessageBox.Yes:
            RemoveDir('cluster/k_means')
            RemoveDir('cluster/Agglomerative_Clustering')
            self.ui2.textBrowser.append("<h4>缓存已清除</h4>")  # 调用append方法可以向文本浏览框中添加文本
        # print(self.clear_thread.isFinished())

    # 界面选择
    def on_pushButton_clicked(self, btn):
        if btn == self.ui2.wordsegmentation:
            self.ui2.textBrowser.append("<h4>" + btn.text() + "界面已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
            self.ui2.stackedWidget.setCurrentIndex(0)
        elif btn == self.ui2.NER:
            self.ui2.textBrowser.append("<h4>" + btn.text() + "界面已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
            self.ui2.stackedWidget.setCurrentIndex(1)
        elif btn == self.ui2.classification:
            self.ui2.textBrowser.append("<h4>" + btn.text() + "界面已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
            self.ui2.stackedWidget.setCurrentIndex(3)
        elif btn == self.ui2.cluster:
            self.ui2.textBrowser.append("<h4>" + btn.text() + "界面已选择</h4>")  # 调用append方法可以向文本浏览框中添加文本
            self.ui2.stackedWidget.setCurrentIndex(2)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    # setup stylesheet
    # apply_stylesheet(app, theme='dark_cyan.xml')
    myWin = Window_main()
    myWin.ui2.show()
    sys.exit(app.exec_())
